import Vue from 'vue';
import Router from 'vue-router';
import SaveTheDate from './views/SaveTheDate';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'save-the-date',
      component: SaveTheDate
    }
  ]
});
